package com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;

/**
 * <p>web项目</p>
 * <p>1. 相当于 web.xml 配置</p>
 *
 * @author wukf
 * @version latest
 * @date 2017/11/21;
 */
@EnableDiscoveryClient
@SpringBootApplication
@ComponentScan(basePackages = {"com.wkf","com.jmapp"})
public class WebApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(WebApplication.class);
    }

}

