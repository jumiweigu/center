package com.jmapp.mvc.filter;

import com.jmapp.staticcode.utils.CenterJwtUtils;
import com.jmapp.staticcode.utils.PathUtil;
import com.wkf.core.domain.result.ErrorResult;
import com.wkf.core.domain.result.ResultEnum;
import com.wkf.util.Toolkit;
import com.wkf.util.security.JwtUtil;
import com.wkf.util.string.JsonMapper;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import lombok.extern.log4j.Log4j;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

/**
 * <p>登录过滤器</p>
 *
 * @author wukf
 * @version latest
 * @date 2016/4/26
 */
@Log4j
public class LoginFilter extends OncePerRequestFilter {

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response,
                                    FilterChain filterChain) throws ServletException, IOException {
        if (PathUtil.isExclusion(request,getFilterConfig())) {
            filterChain.doFilter(request, response);
        } else {
            Map<String, Object> map = CenterJwtUtils.parserToken(request,null);
            if (map == null) {
                //throw new ServletException("NO_LOGIN");
                response.setStatus(480);
                response.getWriter().write("{\"errorCode\":1,\"errorMsg\":\"NO LOGIN\"}");
                return ;
            }
            request.setAttribute(CenterJwtUtils.JWT_MAP, map);
            setCrossDomain(response);
            filterChain.doFilter(request, response);
        }
    }
    private void setCrossDomain(HttpServletResponse response) {
//        response.setHeader("Access-Control-Allow-Origin", "*");
//        response.setHeader("Access-Control-Allow-Methods", "POST, GET,PUT, OPTIONS, DELETE");
//        response.setHeader("Access-Control-Max-Age", "3600");
//        response.setHeader("Access-Control-Allow-Headers", "x-requested-with");
    }

}
