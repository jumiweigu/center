package com.jmapp.mvc.controller;

import com.jmapp.business.api.manger.LoginApi;
import com.jmapp.business.exception.AppException;
import com.jmapp.business.service.system.LoginService;
import com.jmapp.business.service.system.StaffService;
import com.jmapp.mvc.domain.manger.LoginDo;
import com.jmapp.mvc.domain.manger.RegisterDo;
import com.jmapp.mvc.vo.manger.LoginVo;
import com.jmapp.mvc.vo.manger.RegisterVo;
import com.jmapp.mvc.vo.system.ResultVo;
import com.jmapp.repository.po.manger.Staff;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Administrator on 2018/8/7.
 */
@RestController
@RequestMapping("/")
public class LoginController implements LoginApi {
    @Autowired
    private HttpServletRequest request;

    @Autowired
    private LoginService loginService;

    @Autowired
    private StaffService staffService;

    @Override
    @ApiOperation("用户登录")
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public LoginVo login(@RequestBody LoginDo loginDo) throws Exception {
        loginDo.setIp(request.getRemoteHost());
        return loginService.login(loginDo);
    }

    @Override
    @ApiOperation("注册")
    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public RegisterVo register(@RequestBody RegisterDo registerDo) throws Exception {
        return loginService.register(registerDo);
    }

    @Override
    @ApiOperation("发送短信验证码")
    @RequestMapping(value = "/send_code", method = RequestMethod.GET)
    public ResultVo sendCode(@ApiParam("手机号码") @RequestParam(value = "pn") String phoneNumber, @ApiParam("验证码类型") @RequestParam(value = "type") int type) throws Exception {
        ResultVo resultVo=new ResultVo();
//        String sessionCode = (String) request.getSession().getAttribute(phoneNumber);
//        if (StringUtil.isNotEmpty(sessionCode)) {
//            throw new AppException(461);
//        }
//        String code = loginService.sendCode(phoneNumber, type);
//        request.getSession().setAttribute(phoneNumber, code);
//        request.getSession().setMaxInactiveInterval(3 * 60);  //3分钟失效
        resultVo.setCode(0);
        resultVo.setMsg("发送验证码成功");
        return resultVo;
    }

    @Override
    @ApiOperation("校验注册用户")
    @RequestMapping(value = "/check_user", method = RequestMethod.GET)
    public ResultVo checkUser(@ApiParam("手机号码") @RequestParam(value = "pn") String phoneNumber) throws Exception {
        ResultVo resultVo=new ResultVo();
        Staff staff = staffService.findByPhoneNumber(phoneNumber);
        if (staff != null) {
            throw new AppException(462);
        }
        resultVo.setCode(0);
        resultVo.setMsg("该账号可以注册");
        return resultVo;
    }
}
