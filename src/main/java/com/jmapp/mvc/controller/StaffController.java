package com.jmapp.mvc.controller;

import com.jmapp.business.api.manger.StaffApi;
import com.jmapp.business.service.system.MenuService;
import com.jmapp.business.service.system.RoleService;
import com.jmapp.business.service.system.StaffService;
import com.jmapp.mvc.co.manger.StaffCo;
import com.jmapp.mvc.qo.manger.StaffQo;
import com.jmapp.mvc.uo.manger.StaffUo;
import com.jmapp.mvc.vo.manger.StaffVo;
import com.jmapp.mvc.vo.system.ResultVo;
import com.jmapp.repository.po.manger.Menu;
import com.jmapp.repository.po.manger.Role;
import com.jmapp.repository.po.manger.Staff;
import com.jmapp.staticcode.utils.CenterJwtUtils;
import com.wkf.core.domain.page.PageItem;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Administrator on 2018/8/13.
 */
@RestController
@RequestMapping("/staff")
public class StaffController implements StaffApi {

    @Autowired
    private StaffService staffService;

    @Autowired
    private RoleService roleService;

    @Autowired
    private MenuService menuService;

    @Autowired
    private HttpServletRequest request;

    @Override
    @RequestMapping(value = "/menu/", method = RequestMethod.GET)
    public List<Menu> getStaffReource() throws Exception {
        Integer staffId = CenterJwtUtils.getCenterStaffId(request);
//        Staff staff=CenterJwtUtils.getStaff(request);
//        Integer staffId=staff.getStaffId();
        // 后台平台登录用户店铺id 默认为1
        List<Role> roles = roleService.findRole(staffId, 1);
        return menuService.findMenu(roles);
    }

    @Override
    @RequestMapping(value = "/role/", method = RequestMethod.GET)
    public List<Role> getStaffRole() throws Exception {
        Integer staffId = CenterJwtUtils.getCenterStaffId(request);
        // 后台平台登录用户店铺id 默认为1
        return roleService.findRole(staffId, 1);
    }

    @Override
    @RequestMapping(value = "/role", method = RequestMethod.POST)
    public List<Role> findRole(@ApiParam("用户查询对象") @RequestBody StaffQo staffQo) throws Exception {
        return roleService.findRole(staffQo.getStaffId(), staffQo.getShopId());
    }

    @Override
    @RequestMapping(value = "/changerole", method = RequestMethod.POST)
    public ResultVo changeRole(@ApiParam("用户修改对象") @RequestBody StaffUo staffUo) throws Exception {
        ResultVo resultVo = new ResultVo();
        staffService.changeRole(staffUo);
        resultVo.setCode(0);
        resultVo.setMsg("修改用户角色成功");
        return resultVo;
    }

    @Override
    @RequestMapping(value = "", method = RequestMethod.POST)
    public Staff save(@ApiParam("用户创建对象") @RequestBody StaffCo staffCo) throws Exception {
        return staffService.save(staffCo);
    }

    @Override
    @RequestMapping(value = "", method = RequestMethod.PUT)
    public Staff update(@ApiParam("用户修改对象") @RequestBody StaffUo staffUo) throws Exception {
        return staffService.update(staffUo);
    }

    @Override
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResultVo delete(@ApiParam("用户id") @PathVariable("id") Integer staffId) throws Exception {
        ResultVo resultVo = new ResultVo();
        staffService.delete(staffId);
        resultVo.setCode(0);
        resultVo.setMsg("删除成功");
        return resultVo;
    }

    @Override
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public List<StaffVo> findList(@ApiParam("用户查询对象") @RequestBody StaffQo staffQo) throws Exception {
        return staffService.findList(staffQo);
    }

    @Override
    @RequestMapping(value = "/page", method = RequestMethod.POST)
    public PageItem<StaffVo> findPage(@ApiParam("用户查询对象") @RequestBody StaffQo staffQo) throws Exception {
        return staffService.findPage(staffQo);
    }
}
