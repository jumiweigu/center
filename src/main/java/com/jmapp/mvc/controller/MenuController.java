package com.jmapp.mvc.controller;

import com.jmapp.business.api.manger.MenuApi;
import com.jmapp.business.service.system.MenuService;
import com.jmapp.mvc.co.manger.MenuCo;
import com.jmapp.mvc.qo.manger.MenuQo;
import com.jmapp.mvc.uo.manger.MenuUo;
import com.jmapp.mvc.vo.system.ResultVo;
import com.jmapp.repository.po.manger.Menu;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Administrator on 2018/8/13.
 */
@RestController
@RequestMapping("/menu")
public class MenuController implements MenuApi {

    @Autowired
    private HttpServletRequest request;

    @Autowired
    private MenuService menuService;

    @Override
    @RequestMapping(value = "", method = RequestMethod.POST)
    public Menu save(@ApiParam("资源创建对象") @RequestBody MenuCo menuCo) throws Exception {
        return menuService.save(menuCo);
    }

    @Override
    @RequestMapping(value = "", method = RequestMethod.PUT)
    public Menu update(@ApiParam("资源修改对象") @RequestBody MenuUo menuUo) throws Exception {
        return menuService.update(menuUo);
    }

    @Override
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResultVo delete(@ApiParam("资源修改对象") @PathVariable("id") Integer menuId) throws Exception {
        ResultVo resultVo=new ResultVo();
        Menu menu=menuService.delete(menuId);
        resultVo.setCode(0);
        resultVo.setMsg("删除成功");
        return resultVo;
    }

    @Override
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public List<Menu> findList(@ApiParam("资源查询对象") @RequestBody MenuQo menuQo) throws Exception {
        return menuService.findList(menuQo);
    }
}
