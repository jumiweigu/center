package com.jmapp.mvc.controller;

import com.jmapp.business.api.manger.RoleApi;
import com.jmapp.business.service.system.MenuService;
import com.jmapp.business.service.system.RoleService;
import com.jmapp.mvc.co.manger.RoleCo;
import com.jmapp.mvc.qo.manger.RoleQo;
import com.jmapp.mvc.uo.manger.RoleUo;
import com.jmapp.mvc.vo.manger.RoleVo;
import com.jmapp.mvc.vo.system.ResultVo;
import com.jmapp.repository.po.manger.Menu;
import com.jmapp.repository.po.manger.Role;
import com.wkf.core.domain.page.PageItem;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Administrator on 2018/8/30.
 */
@RestController
@RequestMapping("/role")
public class RoleController implements RoleApi {

    @Autowired
    private RoleService roleService;

    @Autowired
    private MenuService menuService;
    @Override
    @RequestMapping(value = "", method = RequestMethod.POST)
    public Role save(@ApiParam("角色修改对象") @RequestBody RoleCo roleCo) throws Exception {
        return roleService.save(roleCo);
    }

    @Override
    @RequestMapping(value = "", method = RequestMethod.PUT)
    public Role update( @ApiParam("角色修改对象") @RequestBody  RoleUo roleUo) throws Exception {
        return roleService.update(roleUo);
    }

    @Override
    @RequestMapping(value = "/menu/{id}", method = RequestMethod.GET)
    public List<Menu> findMenu(@ApiParam("角色修改对象") @PathVariable("id") Integer roleId) throws Exception {
        return menuService.findMenu(roleId);
    }

    @Override
    @RequestMapping(value = "/menu", method = RequestMethod.POST)
    public ResultVo changeMenu( @ApiParam("角色修改对象") @RequestBody  RoleUo roleUo) throws Exception {
        ResultVo resultVo=new ResultVo();
        roleService.changeResource(roleUo);
        resultVo.setCode(0);
        resultVo.setMsg("修改成功");
        return resultVo;
    }

    @Override
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResultVo delete(@ApiParam("角色修改对象") @PathVariable("id") Integer resourceId) throws Exception {
        ResultVo resultVo=new ResultVo();
        roleService.delete(resourceId);
        resultVo.setCode(0);
        resultVo.setMsg("删除成功");
        return resultVo;
    }

    @Override
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public List<RoleVo> findList(@ApiParam("角色修改对象") @RequestBody  RoleQo roleQo) throws Exception {
        return roleService.findList(roleQo);
    }

    @Override
    @RequestMapping(value = "/page", method = RequestMethod.POST)
    public PageItem<RoleVo> findPage(@ApiParam("角色修改对象") @RequestBody RoleQo roleQo) throws Exception {
        return roleService.findPage(roleQo);
    }
}
