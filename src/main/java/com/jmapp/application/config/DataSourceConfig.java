package com.jmapp.application.config;

import com.alibaba.druid.pool.DruidDataSource;
import com.wkf.core.repository.WkfJdbcTemplate;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import javax.sql.DataSource;
import java.sql.SQLException;


/**
 * <p>Druid数据源注入</p>
 *
 * @author wukf
 * @version latest
 * @date 2016/4/26
 */
@Configuration
public class DataSourceConfig {

    @Bean(name = "primaryDataSource")
    @Qualifier("primaryDataSource")
    @Primary
    @ConfigurationProperties(prefix = "spring.datasource")
    public DataSource dataSource() {
        DruidDataSource druidDataSource = new DruidDataSource();
        try {
            druidDataSource.setFilters("stat");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return druidDataSource;
    }

    @Bean
    public WkfJdbcTemplate primaryJdbcTemplate(
            @Qualifier("primaryDataSource") DataSource dataSource) {
        return new WkfJdbcTemplate(dataSource);
    }

}
