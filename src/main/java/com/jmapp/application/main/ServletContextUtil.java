package com.jmapp.application.main;


import org.springframework.stereotype.Repository;
import org.springframework.web.context.ServletContextAware;

import javax.servlet.ServletContext;

/**
 * <p>应用上下文工具类</p>
 *
 * @author wukf
 * @version latest
 * @date 2017/9/5
 */
@Repository
public class ServletContextUtil implements ServletContextAware {

    public static ServletContext servletContext;

    /*@Autowired
    public JmConfig jmConfig;

    @Override
    public void setServletContext(ServletContext servletContext) {
        Constant.THIRD_URL = jmConfig.THIRD_URL;
        Constant.STATIC_URL = jmConfig.STATIC_URL;
        Constant.TPL_CACHE = jmConfig.TPL_CACHE;
        Constant.COMPRESS = jmConfig.COMPRESS;
        Constant.PLAT_FORM = jmConfig.PLAT_FORM;
        Constant.ENVIRONMENT = jmConfig.ENVIRONMENT;
        servletContext.setAttribute("THIRD_URL", Constant.THIRD_URL);
        servletContext.setAttribute("STATIC_URL", Constant.STATIC_URL);
        servletContext.setAttribute("TPL_CACHE", Constant.TPL_CACHE);
        servletContext.setAttribute("COMPRESS", Constant.COMPRESS);
        servletContext.setAttribute("PLAT_FORM", Constant.PLAT_FORM);
        servletContext.setAttribute("basePath",servletContext.getContextPath());
        ServletContextUtil.servletContext = servletContext;
    }
*/
    public static ServletContext getServletContext() {
        return servletContext;
    }

    @Override
    public void setServletContext(ServletContext servletContext) {

    }
}
