package com.jmapp.staticcode.utils;

import com.jmapp.repository.po.manger.Staff;
import com.jmapp.staticcode.constant.Constant;
import org.springframework.beans.BeanUtils;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Map;

/**
 * Created by Administrator on 2018/8/6.
 */
public class CenterJwtUtils extends JwtUtils {
    private static String SECRET = "SECRET_CENTER";


    public  static Staff getStaff(HttpServletRequest request) throws IOException {
        Map<String, Object> jwtMap = parserToken(request,null);
        Staff staff=new Staff();
      Map map=  (Map)jwtMap.get(Constant.CENTER_STAFF);
        BeanUtils.copyProperties(map,staff);
        return  staff;
    }

    public  static Integer getCenterStaffId(HttpServletRequest request) throws IOException {
        Map<String, Object> jwtMap = parserToken(request,null);
        return (Integer) jwtMap.get(Constant.CENTER_STAFF_ID);
    }


}
