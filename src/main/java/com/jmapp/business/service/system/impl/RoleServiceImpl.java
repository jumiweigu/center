package com.jmapp.business.service.system.impl;

import com.jmapp.business.service.system.RoleService;
import com.jmapp.mvc.co.manger.RoleCo;
import com.jmapp.mvc.qo.manger.RoleQo;
import com.jmapp.mvc.uo.manger.RoleUo;
import com.jmapp.mvc.vo.manger.RoleVo;
import com.jmapp.repository.jpa.manger.RoleRepository;
import com.jmapp.repository.jpa.manger.RoleMenuRepository;
import com.jmapp.repository.po.manger.Menu;
import com.jmapp.repository.po.manger.Role;
import com.jmapp.repository.po.manger.RoleMenu;
import com.jmapp.staticcode.converter.CommonConverter;
import com.wkf.core.domain.page.PageItem;
import com.wkf.util.string.SqlUtil;
import com.wkf.util.string.StringUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;


@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private RoleMenuRepository roleResourceRepository;

    @Autowired
    private SqlUtil sqlUtil;


    @Override
    public List<Role> findRole(Integer staffId, Integer shopId) {
        return roleRepository.findRole(staffId, shopId);
    }

    @Override
    public Role save(RoleCo roleCo) throws Exception {
        Role role = CommonConverter.convert(Role.class, roleCo);
        return roleRepository.save(role);
    }

    @Override
    public Role update(RoleUo roleUo) throws Exception {
        Role role = roleRepository.findOne(roleUo.getRoleId());
        BeanUtils.copyProperties(roleUo, role,CommonConverter.getNullPropertyNames(roleUo));
//        role = CommonConverter.convert(Role.class, roleUo);
        return roleRepository.save(role);
    }

    @Override
    @Transactional
    public void changeResource(RoleUo roleUo) throws Exception {
        roleResourceRepository.deleteByRoleId(roleUo.getRoleId());
        List<Menu> resources=roleUo.getMenus();
        List<RoleMenu> list=new ArrayList<RoleMenu>();
        for (Menu item:resources ) {
            RoleMenu roleResource=new RoleMenu();
            roleResource.setMenuId(item.getMenuId());
            roleResource.setRoleId(roleUo.getRoleId());
            list.add(roleResource);
        }
        roleResourceRepository.save(list);

    }

    @Override
    public Role delete(Integer roleId) throws Exception {
        Role role = roleRepository.findOne(roleId);
        role.setStatus(9);
        return roleRepository.save(role);
    }


    @Override
    public List<RoleVo> findList(RoleQo roleQo) throws Exception {
        String sql = "select r.* FROM role r where r.`status`= 0  " + querySql(roleQo);

        return sqlUtil.queryList(sql, RoleVo.class);
    }

    @Override
    public PageItem<RoleVo> findPage(RoleQo roleQo) throws Exception {
        String sql = "select r.* FROM role r where r.`status`= 0  " + querySql(roleQo);
        return sqlUtil.queryPageItem(sql, roleQo.getPage(), roleQo.getPagesize(), RoleVo.class);
    }

    private String querySql(RoleQo roleQo) {
        StringBuffer condition = new StringBuffer("");
        if (roleQo.getType() != 0) {
            condition.append(" and  r.type=" + roleQo.getType() + "");
        }
        if (StringUtil.isNotEmpty(roleQo.getRoleName())) {
            condition.append(" and r.role_name like '%" + roleQo.getRoleName() + "%'");
        }
        return condition.toString();
    }


}
