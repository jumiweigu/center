package com.jmapp.business.service.system.impl;

import com.jmapp.business.service.system.StaffService;
import com.jmapp.mvc.co.manger.StaffCo;
import com.jmapp.mvc.qo.manger.StaffQo;
import com.jmapp.mvc.uo.manger.StaffUo;
import com.jmapp.mvc.vo.manger.StaffVo;
import com.jmapp.repository.jpa.manger.ShopStaffRepository;
import com.jmapp.repository.jpa.manger.StaffRepository;
import com.jmapp.repository.jpa.manger.StaffRoleRepository;
import com.jmapp.repository.po.manger.Role;
import com.jmapp.repository.po.manger.ShopStaff;
import com.jmapp.repository.po.manger.Staff;
import com.jmapp.repository.po.manger.StaffRole;
import com.jmapp.staticcode.converter.CommonConverter;
import com.wkf.core.domain.page.PageItem;
import com.wkf.util.string.SqlUtil;
import com.wkf.util.string.StringUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2018/8/6.
 */
@Service
public class StaffServiceImpl implements StaffService {
    @Autowired
    private StaffRepository staffRepository;
    @Autowired
    private StaffRoleRepository staffRoleRepository;
    @Autowired
    private ShopStaffRepository shopStaffRepository;

    @Autowired
    private SqlUtil sqlUtil;

    @Override
    @Transactional
    public Staff save(StaffCo staffCo) throws Exception {
        Staff staff=new Staff();
        BeanUtils.copyProperties(staffCo, staff,CommonConverter.getNullPropertyNames(staffCo));
//        Staff staff = CommonConverter.convert(Staff.class, staffCo);

        ShopStaff shopStaff = new ShopStaff();
        Staff staff1 = staffRepository.save(staff);
        shopStaff.setShopId(staffCo.getShopId());
        shopStaff.setStaffId(staff1.getStaffId());
        shopStaffRepository.save(shopStaff);
        return staffRepository.save(staff1);
    }

    @Override
    public Staff update(StaffUo staffUo) throws Exception {
        Staff staff=staffRepository.findOne(staffUo.getStaffId());
//        staff=CommonConverter.convert(Staff.class,staffUo);
        BeanUtils.copyProperties(staffUo, staff,CommonConverter.getNullPropertyNames(staffUo));
        return staffRepository.save(staff);
    }

    @Override
    @Transactional
    public void changeRole(StaffUo staffUo) throws Exception {
        staffRoleRepository.deleteByStaffIdAndShopId(staffUo.getStaffId(),staffUo.getShopId());
        List<Role> roles=staffUo.getRoles();
        List<StaffRole> list =new ArrayList<StaffRole>();
        for(Role item :roles){
            StaffRole staffRole=new StaffRole();
            staffRole.setStaffId(staffUo.getStaffId());
            staffRole.setRoleId(item.getRoleId());
            staffRole.setShopId(staffUo.getShopId());
            list.add(staffRole);
        }
        staffRoleRepository.save(list);
    }

    @Override
    public Staff delete(Integer staffId) throws Exception {
        Staff staff=staffRepository.findOne(staffId);
        staff.setStatus(9);
        return staffRepository.save(staff);
    }

    @Override
    public List<StaffVo> findList(StaffQo staffQo) throws Exception {
        String sql = "SELECT s1.* from staff s1 ,shop_staff s2 where  s1.`status`=0 and s1.staff_id=s2.staff_id  " + querySql(staffQo);

        return sqlUtil.queryList(sql, StaffVo.class);
    }

    @Override
    public PageItem<StaffVo> findPage(StaffQo staffQo) throws Exception {
        String sql = "SELECT s1.* from staff s1 ,shop_staff s2 where  s1.`status`=0 and s1.staff_id=s2.staff_id  " + querySql(staffQo);
        return sqlUtil.queryPageItem(sql, staffQo.getPage(), staffQo.getPagesize(), StaffVo.class);
    }

    private String querySql(StaffQo staffQo) {
        StringBuffer condition = new StringBuffer("");
        if (staffQo.getShopId() != 0) {
            condition.append(" and s2.shop_id=" + staffQo.getShopId() + "");
        }
        if (StringUtil.isNotEmpty(staffQo.getNickName())) {
            condition.append(" and s1.nick_name like  '%" + staffQo.getNickName() + "%'");
        }
        if (StringUtil.isNotEmpty(staffQo.getStaffName())) {
            condition.append(" and s1.staff_name like  '%" + staffQo.getStaffName() + "%'");
        }
        if (StringUtil.isNotEmpty(staffQo.getPhoneNumber())) {
            condition.append(" and s1.phone_number like  '%" + staffQo.getPhoneNumber() + "%'");
        }
        return condition.toString();
    }

    @Override
    public Staff findByPhoneNumber(String phoneNumber) throws Exception {
        return staffRepository.findByPhoneNumber(phoneNumber);
    }

    @Override
    public Staff findByPhoneNumberAndPassword(String phoneNumber, String password) throws Exception {
        return staffRepository.findByPhoneNumberAndPassword(phoneNumber, password);
    }
}
