package com.jmapp.business.service.system;

import com.jmapp.mvc.co.manger.RoleCo;
import com.jmapp.mvc.qo.manger.RoleQo;
import com.jmapp.mvc.uo.manger.RoleUo;
import com.jmapp.mvc.vo.manger.RoleVo;
import com.jmapp.repository.po.manger.Role;
import com.wkf.core.domain.page.PageItem;

import java.util.List;

/**
 * Created by Administrator on 2018/8/13.
 */
public interface RoleService {

    List<Role> findRole(Integer staffId, Integer shopId);

    Role save(RoleCo roleCo) throws Exception;

    Role update(RoleUo roleUo) throws Exception;

    void changeResource(RoleUo roleUo) throws Exception;

    Role delete(Integer roleId) throws Exception;

    List<RoleVo> findList(RoleQo roleQo) throws Exception;

     PageItem<RoleVo> findPage(RoleQo roleQo) throws Exception;
}
