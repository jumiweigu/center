package com.jmapp.business.service.system.impl;

import com.jmapp.business.service.system.MenuService;
import com.jmapp.mvc.co.manger.MenuCo;
import com.jmapp.mvc.qo.manger.MenuQo;
import com.jmapp.mvc.uo.manger.MenuUo;
import com.jmapp.repository.jpa.manger.MenuRepository;
import com.jmapp.repository.po.manger.Menu;
import com.jmapp.repository.po.manger.Role;
import com.jmapp.staticcode.converter.CommonConverter;
import com.wkf.util.string.SqlUtil;
import com.wkf.util.string.StringUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Administrator on 2018/8/13.
 */
@Service
public class MenuServiceImpl implements MenuService {

    @Autowired
    private MenuRepository menuRepository;
    @Autowired
    private SqlUtil sqlUtil;

    @Override
    public List<Menu> findMenu(Integer roleId) throws Exception {

        return menuRepository.findMenu(roleId);
    }


    @Override
    public List<Menu> findMenu(List<Role> roles) throws Exception {
        String sql = "select DISTINCT  r.* from  role_menu rr,menu r where r.menu_id=rr.menu_id and  r.`status`=0 and  rr.role_id in (";
        String roleids = "0";
        for (Role item : roles) {
            roleids = roleids + "," + item.getRoleId();
        }
        sql = sql + roleids + ")";
        List<Menu> resources = sqlUtil.queryList(sql, Menu.class);
        return resources;
    }

    @Override
    public List<Menu>
    findList(MenuQo resourceQo) throws Exception {
        String sql="select r.* from menu r where 1=1 "+querySql(resourceQo);
        List<Menu> resources = sqlUtil.queryList(sql, Menu.class);
        return resources;
    }


    private String querySql(MenuQo menuQo){
        StringBuffer condition = new StringBuffer("");
        condition.append(" and r.`status` ="+menuQo.getStatus()+"");
        if(menuQo.getMenuId()!=null && menuQo.getMenuId()!=0){
            condition.append(" and r.menu_id="+menuQo.getMenuId()+"");
        }
        if(StringUtil.isNotEmpty(menuQo.getCode())){
            condition.append(" and r.menu_name like '%"+menuQo.getMenuName()+"%'");
        }
        if(StringUtil.isNotEmpty(menuQo.getCode())){
            condition.append(" and r.`code` like '"+menuQo.getCode()+"%'");
        }
        return condition.toString();
    }
    @Override
    public Menu save(MenuCo resourceCo) throws Exception {
        Menu menu= CommonConverter.convert(Menu.class,resourceCo);
        return menuRepository.save(menu);
    }

    @Override
    public Menu update(MenuUo menuUo) throws Exception {
        Menu menu = menuRepository.findOne(menuUo.getMenuId());
//        resource = CommonConverter.convert(Resource.class,resourceUo);
        BeanUtils.copyProperties(menuUo, menu,CommonConverter.getNullPropertyNames(menuUo));
        return menuRepository.save(menu);
    }

    @Override
    public Menu delete(Integer menuId) throws Exception {
        Menu resource = menuRepository.findOne(menuId);
        resource.setStatus(9);
        return menuRepository.save(resource);
    }


}
