package com.jmapp.business.service.system;

import com.jmapp.mvc.co.manger.MenuCo;
import com.jmapp.mvc.qo.manger.MenuQo;
import com.jmapp.mvc.uo.manger.MenuUo;
import com.jmapp.repository.po.manger.Menu;
import com.jmapp.repository.po.manger.Role;

import java.util.List;

/**
 * Created by Administrator on 2018/8/13.
 */
public interface MenuService {


    List<Menu> findMenu(Integer roleId) throws Exception;


    List<Menu> findMenu(List<Role> roles) throws Exception;


    List<Menu> findList(MenuQo menuQo) throws Exception;

    Menu save(MenuCo menuCo) throws Exception;

    Menu update(MenuUo menuUo) throws Exception;

    Menu delete( Integer resourceId) throws Exception;
}
