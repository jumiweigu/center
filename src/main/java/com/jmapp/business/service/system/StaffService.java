package com.jmapp.business.service.system;

import com.jmapp.mvc.co.manger.StaffCo;
import com.jmapp.mvc.qo.manger.StaffQo;
import com.jmapp.mvc.uo.manger.StaffUo;
import com.jmapp.mvc.vo.manger.StaffVo;
import com.jmapp.repository.po.manger.Staff;
import com.wkf.core.domain.page.PageItem;

import java.util.List;

/**
 * Created by Administrator on 2018/8/6.
 */
public interface StaffService {

    Staff findByPhoneNumber(String phoneNumber)throws Exception;

    Staff findByPhoneNumberAndPassword(String phoneNumber, String password)throws Exception;

    Staff save(StaffCo staffCo)throws Exception;

    Staff update(StaffUo staffUo)throws  Exception;

    void changeRole (StaffUo staffUo) throws Exception;

    Staff delete(Integer staffId)throws Exception;

    List<StaffVo> findList(StaffQo staffQo)throws Exception;

    PageItem<StaffVo> findPage(StaffQo staffQo) throws Exception;

}
