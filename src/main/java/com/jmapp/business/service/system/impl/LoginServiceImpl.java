package com.jmapp.business.service.system.impl;

import com.jmapp.business.exception.AppException;
import com.jmapp.business.service.system.LoginService;
import com.jmapp.business.service.system.StaffService;
import com.jmapp.mvc.co.manger.StaffCo;
import com.jmapp.mvc.domain.manger.LoginDo;
import com.jmapp.mvc.domain.manger.RegisterDo;
import com.jmapp.mvc.vo.manger.LoginVo;
import com.jmapp.mvc.vo.manger.RegisterVo;
import com.jmapp.repository.jpa.log.LoginLogRepository;
import com.jmapp.repository.jpa.manger.ShopStaffRepository;
import com.jmapp.repository.po.log.LoginLog;
import com.jmapp.repository.po.manger.Staff;
import com.jmapp.staticcode.constant.Constant;
import com.jmapp.staticcode.converter.CommonConverter;
import com.jmapp.staticcode.utils.CenterJwtUtils;
import com.wkf.tx.qcloud.SmsUtil;
import com.wkf.util.string.StringUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2018/8/6.
 */
@Service
public class LoginServiceImpl implements LoginService {

    @Autowired
    private StaffService staffService;
    @Autowired
    private LoginLogRepository loginLogRepository;

    @Autowired
    private ShopStaffRepository shopStaffRepository;

    @Override
    public LoginVo login(LoginDo loginDo) throws Exception {
        Staff staff = staffService.findByPhoneNumberAndPassword(loginDo.getPhoneNumber(), loginDo.getPassword());
        if (staff == null) {
            throw new AppException(460);
        }
        createLoginLog(staff,loginDo.getIp());
        return getLoginVo(staff);
    }

    private void  createLoginLog(Staff staff,String ip) throws Exception {
        LoginLog loginLog=new LoginLog();
        loginLog.setUserId(staff.getStaffId());
        loginLog.setType(1);
        loginLog.setUserType(2);
        loginLog.setTime(new Date());
        loginLog.setIpAddr(ip);
        loginLogRepository.save(loginLog);
    };

    @Override
    public RegisterVo register(RegisterDo registerDo) throws Exception {
        RegisterVo registerVo = null;
        if (registerDo.getCode() == null) {
            throw new AppException(433);
        }
        if (registerDo.getPhoneNumber() == null) {
            throw new AppException(433);
        }
        if(!registerDo.getCode().equals(registerDo.getSessionCode())){
            throw new AppException(461);
        }
        Staff staff = staffService.findByPhoneNumber(registerDo.getPhoneNumber());
        if (staff != null) {
            throw new AppException(444);
        }
        StaffCo staffCo = new StaffCo();
        staffCo.setShopId(1);   //后端平台用户店铺id为1
        BeanUtils.copyProperties(registerDo, staffCo,CommonConverter.getNullPropertyNames(registerDo));
        Staff staff2 = staffService.save(staffCo);
        String token = createToken(staff2);
        createLoginLog(staff2,registerDo.getIp());
        BeanUtils.copyProperties(staff2, registerVo,CommonConverter.getNullPropertyNames(staff2));
        registerVo.setToken(token);
        registerVo.setTokenType(CenterJwtUtils.TOKEN_PREFIX);
        return registerVo;
    }

    @Override
    public String sendCode(String phoneNumber, int type) throws Exception {
        String code = StringUtil.getRandom(4);
        int res = SmsUtil.sendLoginCode(phoneNumber,code);
        return code;
    }

    private LoginVo getLoginVo(Staff staff) throws IOException {
        LoginVo loginVo = new LoginVo();
        String userToken = createToken(staff);
        loginVo.setToken(userToken);
        loginVo.setType(CenterJwtUtils.TOKEN_PREFIX);
        return loginVo;
    }

    public String createToken(Staff staff) throws IOException {
        Map<String, Object> map = new HashMap<>();
        map.put(Constant.CENTER_STAFF_ID, staff.getStaffId());
        return CenterJwtUtils.createToken(map);
    }


}
