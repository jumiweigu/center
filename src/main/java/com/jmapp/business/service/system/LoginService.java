package com.jmapp.business.service.system;

import com.jmapp.mvc.domain.manger.LoginDo;
import com.jmapp.mvc.domain.manger.RegisterDo;
import com.jmapp.mvc.vo.manger.LoginVo;
import com.jmapp.mvc.vo.manger.RegisterVo;

/**
 * Created by Administrator on 2018/8/6.
 */
public interface LoginService {

    LoginVo login(LoginDo loginDo)throws Exception;


    RegisterVo register(RegisterDo registerDo) throws Exception;


    String sendCode(String phoneNumber,int type)throws  Exception;


}
