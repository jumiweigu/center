package com.jmapp.business.exception;

import com.netflix.hystrix.exception.HystrixBadRequestException;
import com.wkf.core.domain.result.ErrorResult;
import com.wkf.core.domain.result.ErrorResultUtil;
import com.wkf.core.exception.BusinessException;
import com.wkf.core.exception.WxException;
import lombok.extern.log4j.Log4j;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Writer;

/**
 * @author wukf
 * @version latest
 * @date 2017/11/21
 */
@Log4j
@ControllerAdvice
public class ExceptionHandle {

    @ExceptionHandler(value = {Exception.class})
    @ResponseBody
    public void handler(Exception e, HttpServletRequest request, HttpServletResponse response) throws IOException {
        Writer writer = response.getWriter();
        String msg = "";
        if (e instanceof AppException){
            AppException exception = (AppException) e;
            int status = exception.getStatus();
            response.setStatus(status);
            msg = exception.getMessage();
        }else{
            response.setStatus(466);

        }
        writer.write("");
    }
//    public ErrorResult handle(Exception e, HttpServletRequest request) {
//        if (e instanceof BusinessException) {
//            BusinessException exception = (BusinessException) e;
//            log.error("业务异常：" + e.getMessage(), e);
//            return ErrorResultUtil.fail(exception.getErrmsg());
//        } else if (e instanceof WxException) {
//            WxException exception = (WxException) e;
//            log.error("调用微信接口异常：" + e.getMessage(), e);
//            return ErrorResultUtil.fail(exception.getErrmsg());
//        } else if (e instanceof MissingServletRequestParameterException) {
//            log.error("缺少参数：" + e.getMessage(), e);
//            return ErrorResultUtil.fail("参数错误：" + e.getMessage());
//        } else if (e instanceof HttpRequestMethodNotSupportedException) {
//            log.error("请求方法不存在：" + e.getMessage(), e);
//            return ErrorResultUtil.fail("请求方法不存在：" + e.getMessage());
//        } else if (e instanceof HystrixBadRequestException) {
//            log.error("Feign Error：" + e.getMessage(), e);
//            return ErrorResultUtil.fail("Feign Error:" + e.getMessage());
//        }
//        return null;
//    }

}
