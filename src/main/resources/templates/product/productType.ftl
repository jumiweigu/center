<!DOCTYPE html>
<html>
<head>
    <title>分类列表</title>
<#include "/common/head.ftl" />
    <style>
        /*.container .container-inner.transitioning{*/
        /*-webkit-transform: none;*/
        /*transform: none;*/
        /*}*/
        /*.container .container-inner .content-main{*/
        /*-webkit-transform: none;*/
        /*transform: none;*/
        /*}*/
        /*.m-classifybox{*/
        /*-webkit-transform: none;*/
        /*transform: none;*/
        /*}*/
        /*#gotoTop img{*/
        /*-webkit-transform: translate3d(0, 0, 0);*/
        /*transform: translate3d(0, 0, 0);*/
        /*}*/
    </style>
</head>
<body class="body-bg">
<div class="container" id="container">
    <!--分类目录-->
    <div id="m-navtab">
        <ul class="m-navtab" id="ul-navtab">
            <li id="protype-1" class="z-sel" data-typeid="-1" data-curpage="0" data-totalpage="0" data-mytop="0"><i
                    class="iconfont icon-classify1"></i>全部商品
            </li>
        <#if productGroupList??>
            <#list productGroupList as group>
                <li id="protype${group.groupId}" data-typeid="${(group.groupId)}" data-curpage="0" data-totalpage="0"
                    data-mytop="0">
                    <#if group_index%4==0>
                        <i class="iconfont icon-pointtopl"></i>
                    <#elseif group_index%4==1>
                        <i class="iconfont icon-pointtopr"></i>
                    <#elseif group_index%4==2>
                        <i class="iconfont icon-pointbotr"></i>
                    <#elseif group_index%4==3>
                        <i class="iconfont icon-pointbotl"></i>
                    </#if>
                ${(group.groupName)!}
                </li>
            </#list>
        </#if>
        <#if tradActivityList??>
            <#list tradActivityList as trad>
                <#if trad==1>
                    <li id="protype-2" data-typeid="-2" data-curpage="0" data-totalpage="0" data-mytop="0">
                        <i class="iconfont icon-imageText c-red"></i>一元夺宝
                    </li>
                <#elseif trad==2>
                    <li id="protype-3" data-typeid="-3" data-curpage="0" data-totalpage="0" data-mytop="0">
                        <i class="iconfont icon-imageText c-red"></i>拼团
                    </li>
                </#if>
            </#list>
        </#if>
        </ul>
    </div>
    <!--/分类目录-->
<#--<!--条数显示&ndash;&gt;-->
<#--<div id="showpage" style="border-radius:4px;position:fixed;background:#efefef;margin:auto;bottom:70px;left:0;right:0;z-index: 100;width:50px;text-align: center;padding: 2px 0">-->
<#--<span id="mypage">1</span>/-->
<#--<span id="totalpage">6</span>-->
<#--</div>-->
<#--<!--条数显示&ndash;&gt;-->
    <div class="container-inner transitioning">
        <div class="content-main">
            <!--分类内容-->
            <div class="m-classifybox" id="classifybox">
                <!--全部商品-->
                <ul class="shoplist" id="goods-1">
                    <!--无商品图-->
                    <div class="noshop" style="display: none">
                        <img src="${THIRD_URL}/css/app/img/noshop.png"/>
                        <p>暂无商品</p>
                    </div>
                </ul>
            <#if productGroupList??>
                <#list productGroupList as group>
                    <ul class="shoplist" id="goods${group.groupId}" style="display: none">
                        <!--无商品图-->
                        <div class="noshop" style="display: none">
                            <img src="${THIRD_URL}/css/app/img/noshop.png"/>
                            <p>暂无商品</p>
                        </div>
                    </ul>
                </#list>
            </#if>
            <#if tradActivityList??>
                <#list tradActivityList as trad>
                    <#if trad==1>
                        <ul class="shoplist" id="goods-2" style="display: none">
                            <!--无商品图-->
                            <div class="noshop" style="display: none">
                                <img src="${THIRD_URL}/css/app/img/noshop.png"/>
                                <p>暂无商品</p>
                            </div>
                        </ul>
                    <#elseif trad==2>
                        <ul class="shoplist" id="goods-3" style="display: none">
                            <!--无商品图-->
                            <div class="noshop" style="display: none">
                                <img src="${THIRD_URL}/css/app/img/noshop.png"/>
                                <p>暂无商品</p>
                            </div>
                        </ul>
                    </#if>
                </#list>
            </#if>
            </div>
            <!--/分类内容-->

        </div>
    </div>

    <!--footer-->
<#include "/common/wx_footer.ftl" />
    <!--footer-->
    <a class="sidetop" id="gotoTop" onclick="product.Type.gotop()" style="display: none;">
        <img src="${THIRD_URL}/img/app/mytop.png" alt=""
             style="right:20px;bottom:86px;position:fixed;width:38px;height:38px;z-index: 20">
    </a>
</div>
<script type="text/javascript">
    share.shareShop();
    product.Type.init();
</script>
</body>
</html>